#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Extract one PDF file per applicant in the global postbac file.
`pdftk` needs to be installed.
"""

# 1.9/9.1
# 105 tasks à 46.8s  2.3/
# 2.3/27s
from PyPDF2 import PdfFileWriter, PdfFileReader
import os
import collections
import sys
from joblib import Parallel, delayed

FOLDER_SEP = u'Dossier n'
PAGE_SEP = u'Page '
VERBOSE = 10


def get_dossier_num(line):
    # Get folder value
    # Search the position giving the folder value (the separator can be
    # present several times on the line)
    folder_pos = line.rfind(FOLDER_SEP)
    line = line[folder_pos+1:]
    line =  line[len(FOLDER_SEP):]
    line = line[:line.find(" ")]
    return int(line)


def get_pages_for_dossiers(reader):
    """Loop over the the big datafile and extract, for each applicanat folder,
       its pages of interest"""
    dict = collections.defaultdict(list)
    for num in range(reader.getNumPages()):
        if (num % 200 == 0) & (num != 0):
            print "\r%d%% " % (float(num) / float(reader.getNumPages()) * 100)
        page = reader.getPage(num)
        num_dossier = get_dossier_num(page.extractText().split('\n')[-1])
        dict[num_dossier].append((num, page))

    for num_dossier in dict:
        sorted(dict[num_dossier])

    # Check if the pages of an individual are consecutives
    num = 0
    for num_dossier, page_list in dict.iteritems():
        if (num % 100 == 0) & (num != 0):
            sys.stdout.write('\rPages number extraction checkup: %d%%' % (float(num) / float(len(dict))*100))
            sys.stdout.flush()
        # TODO speed up that WITHOUT using numpy
        for i in range(len(page_list)-1):
            assert(page_list[i+1][0] == (page_list[i][0]+1)), "Un dossier n'a pas stocké ses pages de façon consécutive (erreur de parsing / code à revoir): %s %s %d %d " % (num_dossier, page_list, page_list[i+1][0], page_list[i][0])
        num += 1

    return dict


def extract_dossier(inputname, num_dossier, page_list, outputdir):
    outputname = "%s/%d.pdf" % (outputdir, num_dossier)

    writer = PdfFileWriter()
    for (n, p) in page_list:
        writer.addPage(p)
    with open(outputname, "wb") as f:
        writer.write(f)

    # if status != 0:
    #     sys.stderr.write("Erreur lors de l'extraction des pages")
    #     exit(-1)

    # Check if the extracted pdf is correct
    with open(outputname, "rb") as f:
        data = PdfFileReader(f)
        assert(-1 != data.getPage(0).extractText().split('\n')[-1].find("Page 1"))


def extract_each_dossier(reader, fname, dict, outputdir):
    Parallel(verbose=VERBOSE, n_jobs=1)(
        delayed(extract_dossier)(fname, num_dossier, page_list, outputdir)
        for num_dossier, page_list in dict.iteritems())



if __name__ == '__main__':
    if len(sys.argv) != 3 :
        sys.stderr.write("Usage:\n%s FILE_NAME.PDF DESTINATION\n" % sys.argv[0])
        exit(-1)

    fname = sys.argv[1]
    outputdir = sys.argv[2]

    if not os.path.exists(fname):
        sys.stderr.write("%S does not exists" % fname)

    if not os.path.exists(outputdir):
        os.mkdir(outputdir)

    with open(fname, "rb") as file:
        reader = PdfFileReader(file)

        print "\nRécupération des pages de chaque dossier"
        # Couples de num. dossier / liste de pages
        dict = get_pages_for_dossiers(reader)

        print "\nExtraction de chaque dossier"
        extract_each_dossier(reader, fname, dict, outputdir)

        print "\n%d dossiers extraits" % len(dict)
        # TODO add a summary of the extraction
