# But

L'évaluation des dossiers de candidature postbac (en tout cas dans mon IUT) nécessite de parcourir un PDF de quelques milliers de pages.
Ce script permet d'extraire un PDF par candidature et facilite l'analyse des dossiers.

# Pré-requis

- Python avec les bibliothèques :

    - pyPdf http://pybrary.net/pyPdf/
    - joblib https://github.com/joblib/joblib

- pdftk 

# Fonctionnement

```bash
$ mkdir res
$ python parse_postbac.py dossiers.pdf
```